# Install APP Back Rails Front Vue DB Mongo

## MongoDB Wsl2

```bash
sudo apt update
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org
mongod --version
mkdir -p ~/data/db
sudo mongod --dbpath ~/data/db
```

## Rails Gems

```bash
bundle install
```

## Start APP Back Rails

```bash
rails s
```

## Front with VITE

Go to dir app/frontend/dashboard

```bash
yarn install
```

## Front with Webpack

Go to dir app/frontend/dashboard-webpack

```bash
yarn install
```


## Start APP Front

Go to dir app/frontend/dashboard or app/frontend/dashboard-webpack

```bash
yarn dev
```

# Start with Docker

## Instalar Docker

[Docker](https://www.docker.com/)

## Rails

In the root, run

```bash
docker-compose build
docker-compose up
```
## Front VITE

Go to dir app/frontend/dashboard

```bash
docker-compose build
docker-compose up
```

## Front Webpack

Go to dir app/frontend/dashboard-webpack

```bash
docker-compose build
docker-compose up
```