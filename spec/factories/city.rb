FactoryBot.define do
  factory :city do
    name { Faker::Name.name }
    description { Faker::Name.name }
  end
end