require 'rails_helper'
require 'pry'

RSpec.describe City, type: :model do
  it 'has invalid register  without name' do
    # build(:city, name: nil), City with name nil
    # Instance that's not saved
    # the name is validated in the model
    expect(build(:city, name: nil)).not_to be_valid
  end

  it 'when name have more than of thirty words' do
    expect(build(:user, name: Faker::Lorem.characters(31))).not_to be_valid
  end
end