# spec/controllers/city_controller_spec.rb
# rspec spec/controllers
require 'rails_helper'
require 'pry'

RSpec.describe Api::V1::CityController do
  describe "GET index" do
    it "returns a successful response" do
      get :index
      expect(response).to be_successful
    end

    it "return status 200" do
      expect(response.status).to eq(200)
    end

    # Se usa para especificar que una respuesta devuelve un código de estado deseado
    # Acepta estos argumentos: codigos numericos (209) o tipos de estado generico (:ok, :error, :success)
    it "returns OK status" do
      expect(response).to have_http_status(:ok)
    end
  end

  # Describe indicates what I’m testing
  describe "DELETE destroy" do
    it 'Destroy city' do
      city = FactoryBot.create(:city, name: 'test', description: 'description test')
      binding.pry
      expect(city.destroy).to be true
    end
  end
end