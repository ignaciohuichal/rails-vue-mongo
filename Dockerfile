
# Las imágenes de Docker se crean usando una sucesión de imágenes en capas que se acumulan unas sobre otras.
# El primer paso será añadir la imagen de base para nuestra aplicación,
# que formará el punto de partida de la compilación de la aplicación.

FROM ruby:3.1.1

#A continuación, establezca una variable de entorno para especificar la versión de Bundler:
ENV BUNDLER_VERSION=2.3.9

RUN gem install bundler -v 2.3.9

#Ahora, establezca el directorio de trabajo para la aplicación en el contenedor:
WORKDIR /app

COPY . /app/
ENV  BUNDLE_PATH /gem

# Instalo gemas del proyecto
RUN bundle install

# El uso de una secuencia de comandos de punto de entrada nos permite ejecutar el contenedor como un ejecutable.
ENTRYPOINT ["bin/rails"]
CMD ["s", "-b", "0.0.0.0"]

EXPOSE 3000