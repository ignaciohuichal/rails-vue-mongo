import CityService from '../../service/CityService'
import './style.css'

export default {
  name: "home",
  data() {
    return{ data: undefined,
      city: {name: '', description: ''}
    };
  },
  async mounted() {
    this.data = await CityService.AllCity()
  },
  methods: {
    async addCity() {
      this.data = await CityService.create({
                                              name: this.city.name,
                                              description: this.city.description
                                            })
    },
    async deleteCity(city) {
      this.data = await CityService.delete(city._id.$oid)
    }
  },
  template:
    `
    <div class="container">
      <div v-for="city in data">NOMBRE: {{ city.name }} DESCRIPCION: {{ city.description }}
        <button class="button" v-on:click="deleteCity(city)">Borrar Ciudad</button>
      </div>
    </div>

    <div class="container-new">
      <p> Nombre </p>
      <input class="text-input" v-model="this.city.name" placeholder="Nombre">
      <p> Descripcion </p>
      <input class="text-input" v-model="this.city.description" placeholder="Descripcion">
      <button class="button" v-on:click="addCity()">Crear Ciudad</button>
    </div>
    `
};