import Api from './Api';

export default {
  async AllCity() {
    return await Api.get('/city');
  },
  async create(params) {
    return await Api.post('/city', params);
  },
  async delete(id) {
    return await Api.delete('/city', id);
  }
}