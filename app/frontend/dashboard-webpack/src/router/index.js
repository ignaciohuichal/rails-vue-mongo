import {createRouter, createWebHistory} from 'vue-router'
import Home from '../pages/Home/index.js'
import City from '../pages/City/index.js'
import Route from '../pages/Route/index.js'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/city',
    name: 'city',
    component: City
  },
  {
    path: '/route',
    name: 'route',
    component: Route
  }
]

const router = createRouter({
  history:createWebHistory(),
  routes
})

export default router