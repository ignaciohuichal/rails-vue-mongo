import axios from 'axios';

const BASE_URL = 'http://localhost:3000/api/v1';

export default {
  async get(resource){
    return await axios
      .get(BASE_URL + resource)
      .then(response => response.data)
  },
  async post(resource, body){
    return await axios
      .post(BASE_URL + resource, body)
      .then(response => response.data)
  },
  async delete(resource, id){
    return await axios
      .delete(`${BASE_URL}${resource}/${id}`)
      .then(response => response.data)
  }
}