import Api from './Api';

export default {
  async all() {
    return await Api.get('/route');
  },
  async create(params) {
    return await Api.post('/route', params);
  },
  async delete(id) {
    return await Api.delete('/route', id);
  }
}