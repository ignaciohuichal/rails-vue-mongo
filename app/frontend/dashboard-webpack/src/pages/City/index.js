import CityService from '../../service/CityService'
import template from './template.pug'

export default {
  name: "city",
  data() {
    return{
      data: undefined,
      city: {
              name: '',
              description: ''
            }
    };
  },
  async mounted() {
    this.data = await CityService.all()
  },
  methods: {
    async create() {
      this.data = await CityService.create({
        name: this.city.name,
        description: this.city.description
      })
      this.city.name = undefined
      this.city.description = undefined
    },
    async remove(city) {
      this.data = await CityService.delete(city._id.$oid)
    }
  },
  template: template
};