class Route
  include Mongoid::Document
  include Mongoid::Timestamps

  has_many :bus
  belongs_to :city

  field :name, type: String
  field :description, type: String
end
