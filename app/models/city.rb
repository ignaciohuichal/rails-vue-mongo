class City
  include Mongoid::Document
  include Mongoid::Timestamps

  has_many :route

  field :name, type: String
  field :description, type: String

  validates :name, presence: true, length: { in: 3..30 }
end
