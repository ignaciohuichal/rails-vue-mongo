class Bus
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :route

  field :driver, type: String
end
