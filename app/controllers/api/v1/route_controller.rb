class Api::V1::RouteController < ActionController::Base
  respond_to :json
  skip_before_action :verify_authenticity_token, :only => [:create, :destroy]

  def index
    render json: Route.all, status: :ok
  end

  def create
    City.create!(
      name: params_city[:name],
      description: params_city[:description],
      route: [Route.first]
    )
    render json: City.all, status: :ok
  end

  def destroy
    city.destroy!
    render json: City.all, status: :ok
  end

  def params_city
    params.require(:city).permit(:name, :description)
  end

  def route
    Route.find(params[:id])
  end
end
