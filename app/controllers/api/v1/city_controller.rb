class Api::V1::CityController < ActionController::Base
  respond_to :json
  skip_before_action :verify_authenticity_token, :only => [:create, :destroy]

  def index
    render json: City.all, status: :ok
  end

  def create
    City.create!(
      name: params_city[:name],
      description: params_city[:description],
      route: [Route.first]
    )
    render json: City.all, status: :ok
  end

  def destroy
    city.destroy!
    render json: City.all, status: :ok
  end

  def params_city
    params.require(:city).permit(:name, :description)
  end

  def city
    City.find(params[:id])
  end
end
