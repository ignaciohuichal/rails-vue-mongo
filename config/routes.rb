Rails.application.routes.draw do
  #get 'route/index'
  #get 'bus/index'
  #root 'city#index'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  namespace :api do
    namespace :v1 do
      resources :city
      resources :route
    end
  end
end
